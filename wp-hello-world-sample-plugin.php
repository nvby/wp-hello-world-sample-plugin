<?php
/**
 * WP Hello World - Sample Plugin
 *
 * @link              https://gitlab.com/nvby/wp-hello-world-sample-plugin
 * @since             1.0.0
 * @package           WPHWSP
 *
 * @wordpress-plugin
 * Plugin Name:       WP Hello World - Sample Plugin
 * Plugin URI:        https://gitlab.com/nvby/wp-hello-world-sample-plugin
 * Description:       This is a WordPress "Hello World" sample plugin
 * Version:           1.0.1
 * Author:            Neven Boyanov
 * Author URI:        https://boyanov.org/
 * License:           GPL-2.1+
 * License URI:       https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'WPHWSP_PLUGIN_FILE', __FILE__ );

/**
 * Activation Hook
 */
function wphwsp_activation_hook() {
	//wp_die("Activation Hook");
}
register_activation_hook(WPHWSP_PLUGIN_FILE, 'wphwsp_activation_hook');

/**
 * Deactivation Hook
 */
function wphwsp_deactivation_hook() {
	//wp_die("Deactivation Hook");
}
register_deactivation_hook(WPHWSP_PLUGIN_FILE, 'wphwsp_deactivation_hook');

add_filter( 'login_message', 'wphwsp_login_message' );

/**
 * Adds 'Hello World' above the login form in the WordPress login form.
 *
 * @param    string $message    The default message to display above the form.
 * @return   string $message    The message to display above the form.
 */
function wphwsp_login_message( $message ) {
	$message = '<h2>';
	$message .= 'Hello, World!';
	$message .= '</h2>';
	return $message ;
}
