# WP Hello World - Sample plugin

This is a sample WordPress "Hello World" plugin. It does not do anything useful.

## With the help of ...

Some parts were borrowed from other projects:
- Hello World WordPress Plugin: https://github.com/ashiishme/hello-world-plugin
- Quotes Of The Day: https://github.com/ashiishme/quotes-of-the-day/
- WordPress Hello World: https://github.com/tommcfarlin/wp-hello-world

